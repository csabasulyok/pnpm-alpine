# buildah build -t csabasulyok/pnpm:8.10.0-node20.9.0-alpine .
# buildah login -u csabasulyok
# buildah push csabasulyok/pnpm:8.10.0-node20.9.0-alpine
# buildah run -it --rm csabasulyok/8.10.0-node20.9.0-alpine sh

ARG NODE_VERSION=20.9.0
FROM docker.io/node:${NODE_VERSION}-alpine
ARG PNPM_VERSION=8.10.0
RUN apk add curl git openssh-client jq
RUN npm install --global pnpm@${PNPM_VERSION}
RUN pnpm config set store-dir .pnpm-store
